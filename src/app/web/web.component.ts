import { Component, OnInit } from '@angular/core';
import { ApiManagerService } from './../api-manager.service';
import {AgmMap,MapsAPILoader  } from '@agm/core';

@Component({
	selector: 'app-web',
	templateUrl: './web.component.html',
	styleUrls: ['./web.component.css']
})
export class WebComponent implements OnInit {

	loading: boolean = true;
	data: any = null;
  error = null;
  galleries:any=null;

  modal_data: any = {};
  latitude: number=-1.912587;
  longitude: number=30.089200;
  zoom:number;
     center: {lat: 34.075328, lng: -118.330432}
  selectedMarker;
  mapType = 'satellite';
  markers =    [ { lat: this.latitude, lng:this.longitude, alpha: 1 }]

	images = ["assets/images/slide1.jpg", "assets/images/slide2.jpg","assets/images/slide3.jpg"];
	i = 0;
	current_image = this.images[0];

	constructor(private api: ApiManagerService) {

   }

	ngOnInit() {

    this.makeCarousel();
    this.setCurrentLocation();
    this.getPhoto();




  }



  location(event){
    this.latitude=event.coords.lat;
    this.longitude=event.coords.lng;
    console.log(this.longitude,this.latitude)
  }

  getPhoto(){
		let obs = this.api.getGallery();
		obs.subscribe((response) => {

			this.loading = true;
      this.galleries = response;
      if(this.galleries){
        this.loading = false;
      }


      console.log("galleries",this.galleries)

		}, error =>{
			this.loading = false;
			this.galleries = null;
			this.error = error;
		});
	}
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude  = position.coords.longitude;
        this.zoom = 15;
         this.center= {lat: 34.075328, lng: -118.330432};

        console.log("latitude",this.latitude)
        console.log("longitude",this.longitude)

      });
    }

  }











	makeCarousel(){
		setTimeout(() => {
			this.current_image = this.images[this.i];
			this.i++;

			if(this.i >= this.images.length){
				this.i = 0;
			}
			this.makeCarousel();
		 }, 2000);
	}

}
